# 315-game-SuperPollo
315 game: Pulsar Terminator


#Team Members:
Nicole Ahles: ahles_nicole@tamu.edu
Gregorio Ines: gji76@tamu.edu
Max McKinley: MaxWMcKinley@gmail.com
Elliott Dobbs: elliottdobbs@tamu.edu


#Requirements to Run the game
-Modern Operating System
    -Has been tested on Windows 7, 10, and Debian Linux
-Node.js 4.3.2
-Google Chrome
-(The game "probably" can run on other browsers and operating systems, but to be safe use what we have laid out here.)


#Instructions to Run the game
To run the server, we used node.js. During development, we used version 4.3.2. We also ran it on our local machines. After downloading the repository, the server can be started by entering:

node server.js

It should display a message describing the port number it is operating. We used port 30000. For this functional prototype we recommend connecting only two players to the server. Also, after each game, it is recommended that the server be restarted before playing again. It is also recommended that one client clicks find game, and then the other client connects to the server and click find game.

After entering <address_to_server>:3000 into the URL bar of Chrome, the game should pop up with its main menu. In our case <address_to_server> was localhost. To begin looking for a game, click the large find game button. The second client should now connect to the server and click find game.

After playing, it is recommend that the clients both close their tabs. The server should also be restarted.


#Repository Structure
-index_files
     We aren't actually sure what this is, but everytime we delete it, it deletes index.html along with it, which we need. The two scripts located here are also located in the static folder as described later.

-node_modules
    Contains all the packages that are required to run our server with node.js

-pseudocode
    Contains the pseudocode used for deliverable 3 to descibe the basic overall structure of our code (DOES NOT ACTUALLY RUN)

-static
    -assets
        Contains all of the images and media files used for the interface and game itself
    -game
        Contains the script to actually run the game (game.js)
    -phaser
        Contains the phaser.min.js script, which is the phaser engine script that the game is built on

-index.html
    The html file that the server sends to the client whenever a client connects initial

-package.json
    Tells node what resources it needs to run in the server
    
-README.md
    The thing that you are reading

-server.js
    The script that runs are server through node.js
