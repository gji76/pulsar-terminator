var game = new Phaser.Game(800, 600, Phaser.AUTO, 'Pulsar Terminator', { preload: preload, create: create, update: update });

var ship;
var rocks;

var fireRate = 100;
var nextFire = 0;

var Wkey;
var Akey;
var Skey;
var Dkey;

function preload() {
    
    game.load.image('background', 'media/background.png');
    game.load.image('bigRock', 'media/bigRock.png');
    game.load.image('smallRock', 'media/smallRock.png');
    game.load.image('blueShip', 'media/blueShip.png');
    game.load.image('greenShip', 'media/greenShip.png');
    game.load.image('bullet', 'media/blueBullet.png');
    
}

function create() {
    
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    game.add.sprite(0, 0, 'background');
    
    rocks = game.add.group();
    rocks.enableBody = true;
    
    var rock = rocks.create(game.world.width/2-(164/2), 120, 'bigRock');
    rock.body.immovable = true;
    rock = rocks.create(game.world.width/2-(164/2), game.world.height-180, 'bigRock');
    rock.body.immovable = true;
    rock = rocks.create(100,game.world.height/2,'smallRock');
    rock.body.immovable = true;
    rock = rocks.create(game.world.width-100,game.world.height/2,'smallRock');
    rock.body.immovable = true;
    
    ship = game.add.sprite(100, 200, 'blueShip');
    ship.scale.setTo(0.5, 0.5);
    ship.anchor.setTo(0.5, 0.5);
    game.physics.arcade.enable(ship);
    ship.body.collideWorldBounds = true;
    
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(5, 'bullet', 0, false);
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 0.5);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);
    
    Wkey = game.input.keyboard.addKey(Phaser.Keyboard.W);
    Akey = game.input.keyboard.addKey(Phaser.Keyboard.A);
    Skey = game.input.keyboard.addKey(Phaser.Keyboard.S);
    Dkey = game.input.keyboard.addKey(Phaser.Keyboard.D);
}

function update() {
    
    game.physics.arcade.collide(ship, rocks);
    game.physics.arcade.collide(ship, bullets);
    game.physics.arcade.collide(bullets, rocks);
    game.physics.arcade.collide(bullets, ship);

    Wkey = game.input.keyboard.addKey(Phaser.Keyboard.W);
    Akey = game.input.keyboard.addKey(Phaser.Keyboard.A);
    Skey = game.input.keyboard.addKey(Phaser.Keyboard.S);
    Dkey = game.input.keyboard.addKey(Phaser.Keyboard.D);
    
    if(Wkey.isDown){
        ship.body.velocity.y = -150;
    }
    else if(Skey.isDown){
        ship.body.velocity.y = 150;
    }
    else if (Akey.isDown)
    {
        ship.body.velocity.x = -150;
    }
    else if (Dkey.isDown)
    {
        ship.body.velocity.x = 150;
    }
    else
    {
        ship.body.velocity.x = 0;
        ship.body.velocity.y = 0;
    }
    

    ship.rotation = game.physics.arcade.angleToPointer(ship) + 1.57; // 1.57 = pi/2
    
    
    if (game.input.activePointer.isDown)
    {
        if (game.time.now > nextFire && bullets.countDead() > 0)
        {
            nextFire = game.time.now + fireRate;
            var bullet = bullets.getFirstExists(false);
            bullet.body.bounce.set(1);
            bullet.reset(ship.x, ship.y);
            bullet.rotation = game.physics.arcade.moveToPointer(bullet, 500, game.input.activePointer);
        }
    }
    
}
