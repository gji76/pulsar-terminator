var game = new Phaser.Game(800, 600, Phaser.AUTO, 'Pulsar Terminator',
						 { preload: preload, create: create, update: update });


// determines whether or not the client is on a mobile device.
// SOURCE: https://github.com/kaimallea/isMobile
!function(a){var b=/iPhone/i,c=/iPod/i,d=/iPad/i,e=/(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,f=/Android/i,g=/(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i,h=/(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i,i=/IEMobile/i,j=/(?=.*\bWindows\b)(?=.*\bARM\b)/i,k=/BlackBerry/i,l=/BB10/i,m=/Opera Mini/i,n=/(CriOS|Chrome)(?=.*\bMobile\b)/i,o=/(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,p=new RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)","i"),q=function(a,b){return a.test(b)},r=function(a){var r=a||navigator.userAgent,s=r.split("[FBAN");return"undefined"!=typeof s[1]&&(r=s[0]),s=r.split("Twitter"),"undefined"!=typeof s[1]&&(r=s[0]),this.apple={phone:q(b,r),ipod:q(c,r),tablet:!q(b,r)&&q(d,r),device:q(b,r)||q(c,r)||q(d,r)},this.amazon={phone:q(g,r),tablet:!q(g,r)&&q(h,r),device:q(g,r)||q(h,r)},this.android={phone:q(g,r)||q(e,r),tablet:!q(g,r)&&!q(e,r)&&(q(h,r)||q(f,r)),device:q(g,r)||q(h,r)||q(e,r)||q(f,r)},this.windows={phone:q(i,r),tablet:q(j,r),device:q(i,r)||q(j,r)},this.other={blackberry:q(k,r),blackberry10:q(l,r),opera:q(m,r),firefox:q(o,r),chrome:q(n,r),device:q(k,r)||q(l,r)||q(m,r)||q(o,r)||q(n,r)},this.seven_inch=q(p,r),this.any=this.apple.device||this.android.device||this.windows.device||this.other.device||this.seven_inch,this.phone=this.apple.phone||this.android.phone||this.windows.phone,this.tablet=this.apple.tablet||this.android.tablet||this.windows.tablet,"undefined"==typeof window?this:void 0},s=function(){var a=new r;return a.Class=r,a};"undefined"!=typeof module&&module.exports&&"undefined"==typeof window?module.exports=r:"undefined"!=typeof module&&module.exports&&"undefined"!=typeof window?module.exports=s():"function"==typeof define&&define.amd?define("isMobile",[],a.isMobile=s()):a.isMobile=s()}(this);

var onMobile = isMobile.any;

var socket = io();
var socketID = 0;
var myGameID = 0;
var playerColor = 0;
var gamestart = 0;
var myColor = 'none';
var tempEnemyRotation = 0;
var tempEnemyHealth = -1;
var tempEnemyX = -1;
var tempEnemyY = -1;
var tempEnemyBullets = [-1, -1, -1, -1, -1];

var mineBlewUp = false;

///////////////////////////////////////////////////////
// Server actions
///////////////////////////////////////////////////////
socket.on('clientConnected',function(sockID){
	
	// If your socketID is undefined, get it from the
	// server
	if(socketID == 0){
		socketID = sockID;
		console.log('My socketID is ' + socketID);
	}
	
});

socket.on('mineBlewUp',function(bool){
	
	console.log('mineBlewUp received!!!');
	mineBlewUp = true;
	mine.active = false;
	explosion.visible = true;
	explosion.x = mine.x;
	explosion.y = mine.y;
	explosion.animations.play('explode');
	mine.kill();
	
});

socket.on('colorAssignment', function(message){
    
    // If I do not have a color, take the color
	if(myColor == 'none'){

		myColor = message;

        // changing text so player knows which color they are
        colorIndicatorText.text = 'Your color is: ' + myColor;
        if(myColor == 'blue'){

            colorIndicatorText.addColor("#0066ff",15);

        }else{

            colorIndicatorText.addColor("#009900",15);

        }
		console.log('my color is ' + myColor);

	}
	
});

socket.on('gameStarted', function(gameID){
	
	backButton.visible = false;
    lookingForPlayerTitleSprite.visible = false;
    colorIndicatorText.visible = true;

	//console.log('DEBUG on gameStarted shipHealth: ' + playerList[0].ship.health + ' ' + playerList[1].ship.health);
	playerList[0].ship.health = 100;
	playerList[1].ship.health = 100;
	// change the text
	if(myColor == 'blue'){
		
		playerHealthText.text = 'Health: ' + playerList[0].ship.health;
        enemyHealthText.text = 'Health: ' + playerList[1].ship.health;
        blueHealthIndicator.reset(100, 2);
        greenHealthIndicator.reset(670, 2);
		
	}else if(myColor == 'green'){
		
		playerHealthText.text = 'Health: ' + playerList[1].ship.health;
        enemyHealthText.text = 'Health: ' + playerList[0].ship.health;
		greenHealthIndicator.reset(100, 2);
        blueHealthIndicator.reset(670, 2);
        
	}
	// change the graphic
	myHealthBar.loadTexture('gHlthBr');
	myHealthBar.x = 0;
	theirHealthBar.loadTexture('gHlthBr');
	theirHealthBar.x = 0;
	backgroundMusic.volume = .1;
    myGameID = gameID;
    console.log('Client received gameID: ' + myGameID);
	//gamestart = 1;
	myHealthBar.visible = false;
	theirHealthBar.visible = false;
    //startGame();
	countdownTimer.visible = true;
	countdownTimer.animations.play('count');
    blueHealthIndicator.visible = false;
    greenHealthIndicator.visible = false;
	
});

socket.on('updatedState',function(coordinates){

	// Based on information from the server, update the other players state.
	// blue should be 0, green should be 2
	if(myColor == 'blue'){

        tempEnemyRotation = coordinates[2][4];
		tempEnemyX = coordinates[2][0];
		tempEnemyY = coordinates[2][2];
        tempEnemyVelocity = [coordinates[2][1], coordinates[2][3]];
        tempEnemyBullets = coordinates[3];
        tempEnemyPowerUps = coordinates[2][5];
	
	}else if(myColor == 'green'){

        tempEnemyRotation = coordinates[0][4];
		tempEnemyX = coordinates[0][0];
		tempEnemyY = coordinates[0][2];
        tempEnemyVelocity = [coordinates[0][1], coordinates[0][3]];
        tempEnemyBullets = coordinates[1]; // array of an array
        tempEnemyPowerUps = coordinates[0][5];

	}
	
	// Only take care of the mine if the mine has not blown up.
	if(!mineBlewUp){
		
		var tempMineXCoordinate = coordinates[4][0];
		var tempMineYCoordinate = coordinates[4][1];
		var tempMineActive = coordinates[4][3];
		
		// Because each client is in charge of reporting whether or not they get
		// hit by something, only the client who did not drop the mine needs to
		// update the state of the mine
		if((coordinates[4][2] != myColor) && (coordinates[4][2] != 'none') &&
			(mine.active != tempMineActive)){

			// If there is a change in the state of the mine, update the state,
			// also if the mine is active set the position
			console.log('The values were not the same!');
			mine.active = tempMineActive;
			if(mine.active){
				console.log('setting coordinates of the mine!');
				mine.x = tempMineXCoordinate;
				mine.y = tempMineYCoordinate;
				mine.color = coordinates[4][2];
				mine.animations.play('fade',true);
				mine.visible = true;
				
			}
			
		}
	}

});

socket.on('healthUpdated', function(message){
    
    var tempBlueHealth = message[0];
    var tempGreenHealth = message[1];
    
    playerList[0].ship.health = tempBlueHealth;
    playerList[1].ship.health = tempGreenHealth;
});

socket.on('gameEnded', function(color){

	mine.active = false;
	separationBar.visible = false;
	mineIcon.visible = true;

	if(onMobile){
	    // disabling the virtual controls
		virtWButton.visible = false;
		virtAButton.visible = false;
		virtSButton.visible = false;
		virtDButton.visible = false;
		virtSpecialButton.visible = false;
		// 400 sidisables that it can be activated on the right side of the screen
		game.vjoy.inputDisable();
	}

	// hiding the gameplay graphics
	myHealthBar.visible = false;
	theirHealthBar.visible = false;
    playerHealthText.visible = false;
    enemyHealthText.visible = false;
    blueHealthIndicator.visible = false;
    greenHealthIndicator.visible = false;
    satellite.visible = false;

	// resetting the game variables
    myGameID = 0;
    gamestart = 0;
    
    // play the destructive explosion, and reincreasing main soundtrack volume
    deathExplosion.play();
    backgroundMusic.volume = 1;

    // presenting to the client the proper state
	if(color == myColor){

		showDefeat();

	}else{

		showVictory();

	}

    // Reset the health of the ships
    console.log('Resetting their health.');
	playerList[0].ship.reset(50,100,100);
	playerList[1].ship.reset(700,500,100);
	mine.reset(0,0);
	mine.visible = false;
    for(var i in playerList){
        playerList[i].ship.visible = false;
        playerList[i].usingPowerUp = false;
        playerList[i].currentPowerUp = false;
    }

    // reset power ups
    shieldIcon.reset(game.world.width/2, game.world.height/2);
    shieldIcon.visible = false;
    healthIcon.reset(200, 200);
    healthIcon.visible = false;
	
	mineIcon.reset(game.world.width/2 + 50, game.world.height/2);
	mineIcon.visible = false;
    
	// Reset their color
	myColor = 'none';

});

///////////////////////////////////////////////////////

// Background
var background;
var stars;

// Title Sprites
var gameTitleSprite;
var controlsTitleSprite;
var defeatTitleSprite;
var instructionsTitleSprite;
var lookingForPlayerTitleSprite;
var powerUpsTitleSprite;
var victoryTitleSprite;

// Description Sprites
var instructionsDescSprite;
var controlsDescSprite;
var powerUpsDescSprite;

// Buttons
var controlsButton;
var findGameButton;
var instructionsButton;
var instructions2Button;
var mainMenuButton;
var newGameButton;
var powerUpsButton;
var backButton;

// Healthbars
var myHealthBar;
var theirHealthBar;
var separationBar;

// Powerups
var mineIcon;
var mine;
var mineAnimation;

// Text
var playerHealthText;
var enemyHealthText;
var colorIndicatorText;

// Gameplay Variables
var playerList = [];
var playerID = socketID;
var player; 
var rocks;

var fireRate = 250;
var nextFire = 0;

// Input Variables
var Wkey;
var Akey;
var Skey;
var Dkey;

// Music Variables
var backgroundMusic;
var pew;
var deathExplosion;
var damageTaken;

// Animation Variables
var countdownTimer;
var anim;
var explosion;
var explosionAnimation;

// velocity variables
var satelliteVelocity = 50;

// Virtual Buttons
var virtWButton;
var virtAButton;
var virtSButton;
var virtDButton;
var virtSpecialButton;

// keeps track of what buttons were pressed.
var virtWDown;
var virtADown;
var virtSDown;
var virtDDown;
var virtSpecialDown;


///////////////////////////////////////////////////////
// Ship (Player) Class and Functions
///////////////////////////////////////////////////////

function Ship(playerID, game, playerColor){
    
    // Player Variables
    this.ID = playerID;
    this.nextFire = 0;
    this.alive = true;
    this.color = playerColor;
    this.currentPowerUp = false;
    this.usingPowerUp = false;
    this.shieldHits = 2;
    this.x;
    this.y;
    
    // Defining input
    this.input = {
		left: game.input.keyboard.addKey(Phaser.Keyboard.A),
		right: game.input.keyboard.addKey(Phaser.Keyboard.D),
		up: game.input.keyboard.addKey(Phaser.Keyboard.W),
        down: game.input.keyboard.addKey(Phaser.Keyboard.S),
		fire: game.input.activePointer.leftButton,
        powerUp: game.input.activePointer.rightButton
	}
    
    // Creating the Ship and Bullets in the correct color
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        
    if(playerColor == 'blue'){
        this.x = 100;
        this.y = 100;
        this.ship = game.add.sprite(this.x, this.y, 'blueShip');
        this.bullets.createMultiple(3, 'blueBullet', 0, false);
        
    }
    else{ // (playerColor == 'green')
        this.x = 650;
        this.y = 500;
        this.ship = game.add.sprite(this.x, this.y, 'greenShip');
        this.bullets.createMultiple(3, 'greenBullet', 0, false);
        
    }
    
    // Bullet properties
    this.bullets.setAll('anchor.x', 0.5);
    this.bullets.setAll('anchor.y', 0.5);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);

    // Ship properties
    this.ship.scale.setTo(0.5, 0.5);
    this.ship.anchor.set(0.5, 0.5);
    game.physics.arcade.enable(this.ship);
    this.ship.body.collideWorldBounds = true;
    this.ship.body.bounce.setTo(0.3, 0.3);
    
    // Initialize Player Health
    this.ship.health = 100;
    
}

Ship.prototype.update = function() {
    
	// cursors contains information about the direction of the joystick
	// virtButtons contains information about which of the on screen
	//	buttons are being pressed.
	var cursors = {};
	var virtButtons = {};
	if(onMobile){

		cursors = game.vjoy.cursors;

		virtButtons.w = virtWDown;
		virtButtons.a = virtADown;
		virtButtons.s = virtSDown;
		virtButtons.d = virtDDown;
		virtButtons.special = virtSpecialDown;

	}else{

		cursors.left = false;
		cursors.right = false;
		cursors.down = false;
		cursors.up = false;
		cursors.fire = false;

	}
	
    // Update ship movement
	if(!onMobile){
	    this.ship.rotation = game.physics.arcade.angleToPointer(this.ship) + 1.57; // 1.57 = pi/2
	}else{
		this.ship.rotation = game.vjoy.angle + 1.57;
	}
    
    if (this.input.left.isDown || virtButtons.a){
        
        this.ship.body.velocity.x = -120;
    }
    else if (this.input.right.isDown || virtButtons.d){
        
        this.ship.body.velocity.x = 120;
    }
    
    if (this.input.up.isDown || virtButtons.w){
        
        this.ship.body.velocity.y = -120;
    }
    else if (this.input.down.isDown || virtButtons.s){
        
        this.ship.body.velocity.y = 120;
    }
    
    if(!(this.input.left.isDown || this.input.right.isDown) && Math.abs(this.ship.body.velocity.x) > 0){
       if(this.ship.body.velocity.x > 0){
		   
		   this.ship.body.velocity.x -= 1;
		   
	   }else{
		   
		   this.ship.body.velocity.x += 1;
		   
	   }
    }
    if(!(this.input.up.isDown || this.input.down.isDown) && Math.abs(this.ship.body.velocity.y) > 0){
        if(this.ship.body.velocity.y > 0){
		   
		   this.ship.body.velocity.y -= 1;
		   
	   }else{
		   
		   this.ship.body.velocity.y += 1;
		   
	   }
    }

    if (this.input.fire.isDown || cursors.fire)
    {	
		if (game.time.now > nextFire && this.bullets.countDead() > 0)
        {
            
            nextFire = game.time.now + fireRate;
            var bullet = this.bullets.getFirstExists(false);
            bullet.body.bounce.set(1);
            bullet.reset(this.ship.x, this.ship.y);
			if(onMobile){
				bullet.rotation = game.physics.arcade.moveToXY(bullet,
																this.ship.x + Math.cos(game.vjoy.angle),
																this.ship.y + Math.sin(game.vjoy.angle),
																 400);
			}else{
	            bullet.rotation = game.physics.arcade.moveToPointer(bullet, 400, game.input.activePointer);
			}
            pew.play();
        }
    }
    
    if(this.input.powerUp.isDown || virtButtons.special){
		if(this.currentPowerUp == 'shield'){
			
			this.usingPowerUp = true;
			shieldInUse.reset(this.ship.x, this.ship.y);
			
		}else if(this.currentPowerUp == 'mine'){
			
			// get rid of icon
			mineIcon.visible = false;
			this.currentPowerUp = false;
			
			// drop the mine at current position
			mine.x = this.ship.x;
			mine.y = this.ship.y;
			mine.animations.play('fade',true);
			mine.visible = true;
			mine.active = true;
			
		}
		else if(this.currentPowerUp == 'health'){
			
			socket.emit('healthChange', [myGameID, this.color, 20]);
			healthIcon.kill();
			this.currentPowerUp = false;
			this.usingPowerUp = false;
			
		}
			
    }
    
    // Check if the shield has expired
    if(this.shieldHits <= 0){
        
        this.shieldHits = 2; // reset shield health
        this.currentPowerUp = false;
        this.usingPowerUp = false;
        shieldInUse.visible = false;
        shieldIcon.reset(game.world.width/2, game.world.height/2);
        shieldIcon.visible = false;
    }
    
    // Update shield graphic
    if(this.usingShield()){

        shieldInUse.x = this.ship.x;
        shieldInUse.y = this.ship.y;
    }
}

Ship.prototype.bulletHitPlayer = function(shipHit, bullets) {

    shipHit.body.velocity.x = 0;
    shipHit.body.velocity.y = 0;
    bullets.kill();
    
    if(!this.usingShield()){
        
        damageTaken.play();
        shipHit.health -= 10;
        socket.emit('healthChange', [myGameID, this.color, -10]);
    }
    else{
        this.shieldHits -= 1;
    }
}

Ship.prototype.mineHitPlayer = function(shipHit, mine) {

	console.log('mineHitPlayer CALLED!!!!!');
    shipHit.body.velocity.x = 0;
    shipHit.body.velocity.y = 0;
	mine.active = false;
	mine.visible = false;
    mine.kill();
	
    if(!this.usingShield()){
        
        damageTaken.play();
        shipHit.health -= 30;
        socket.emit('healthChange', [myGameID, this.color, -30]);
    }
    else{
        this.shieldHits -= 1;
    }
	
	mineBlewUp = true;
	
}

Ship.prototype.gainPowerUp = function(ship, powerUp){
    
    this.currentPowerUp = powerUp.name;
    powerUp.x = 10;
    powerUp.y = 28;
    
	if(powerUp.name == 'mine')
		mine.color = myColor;

}

Ship.prototype.usingShield = function(){
    
    if(this.currentPowerUp == 'shield' && this.usingPowerUp == true){

        return true;
    }
    else{
        
        return false;
    }
}

Ship.prototype.opponentBulletHit = function(ship, bullets){
    
    if(this.usingShield()){
        
        this.shieldHits -= 1;
    }
    
    damageTaken.play();
    ship.body.velocity.x = 0;
    ship.body.velocity.y = 0;
    bullets.kill();
}


////////////////////////////////////////////////
// Preload, Create and Update Functions
////////////////////////////////////////////////

function preload() {
    
    // Audio
    game.load.audio('bgMusic', 'assets/bensound-scifi.mp3');
    game.load.audio('pew','assets/Audio/pew.mp3');
	game.load.audio('deathExp','assets/Audio/deathExplosion.mp3');
	game.load.audio('dmgTaken','assets/Audio/hit.mp3');

    // Background image
    if(onMobile){

    	game.load.image('background', 'assets/background.png');

	}else{
    
    	game.load.image('stars','assets/stars.png');
    	game.load.image('noStarsBackground','assets/noStarsBackground.png');

    }
    
    // Title images
    game.load.image('gameTitle', 'assets/Titles/GameTitle.png');
    game.load.image('controlsTitle','assets/Titles/ControlsTitle.png');
    game.load.image('defeatTitle','assets/Titles/DefeatTitle.png');
    game.load.image('instructionsTitle','assets/Titles/InstructionsTitle.png');
    game.load.image('lookingForPlayerTitle','assets/Titles/LookingForPlayerTitle.png');
    game.load.image('powerUpsTitle','assets/Titles/PowerUpsTitle.png');
    game.load.image('victoryTitle','assets/Titles/VictoryTitle.png');
    
    // Description images
    game.load.image('instructionsDescription', 'assets/Instructions.png');
    game.load.image('controlsDescription', 'assets/Controls.png');
    game.load.image('powerUpsDescription', 'assets/Power ups.png');
    
    // Button images
    game.load.image('controls','assets/Buttons/ControlsButton.png');
    game.load.image('findGame', 'assets/Buttons/FindGameButton.png');
    game.load.image('instructions', 'assets/Buttons/InstructionsPageButton.png');
    game.load.image('instructions2','assets/Buttons/InstructionsButton.png');
    game.load.image('mainMenu','assets/Buttons/MenuButton.png');
    game.load.image('newGame','assets/Buttons/NewGameButton.png');
    game.load.image('powerUps','assets/Buttons/PowerUpsButton.png');
    game.load.image('back','assets/Buttons/BackButton.png');
    game.load.image('selectedButton', 'assets/Buttons/SelectedButton.png');
    
    // Gameplay images
    game.load.image('bigRock', 'assets/bigObstacle.png');
    game.load.image('smallRock', 'assets/smallObstacle.png');
    game.load.image('satellite', 'assets/satellite.png');
    game.load.image('blueShip', 'assets/blueShip.png');
    game.load.image('greenShip', 'assets/greenShip.png');
    game.load.image('blueBullet', 'assets/blueBullet.png');
    game.load.image('greenBullet', 'assets/greenBullet.png');
	
	// HUD
    game.load.image('gHlthBr', 'assets/greenHealthBar.png');
    game.load.image('rHlthBr', 'assets/redHealthBar.png');
    game.load.image('yHlthBr', 'assets/yellowHealthBar.png');
	game.load.image('separationBar','assets/separationBar.png');
	
	// Power ups
    game.load.image('shieldIcon', 'assets/PowerUps/ShieldPU.png');
    game.load.image('shieldInUse', 'assets/PowerUps/ShieldInUse.png');
	game.load.image('mineIcon', 'assets/PowerUps/MinePU.png');
	game.load.image('mine', 'assets/PowerUps/redMine.png');
    game.load.image('healthIcon', 'assets/PowerUps/HealthPU.png');
    
	// Animations
	game.load.atlasJSONHash('countdownAtlas', 'assets/ss.png', 'assets/ss.json');
	game.load.atlasJSONHash('explosionAtlas', 'assets/explosion_ss.png', 'assets/explosion_ss.json');
	game.load.atlasJSONHash('mineAtlas', 'assets/mineFadeAnimation.png', 'assets/mineFadeAnimation.json');

	// Virtual Joy stick AND virtual buttons, only load if used by a mobile device
	if(onMobile){
		game.load.image('vjoy_base', 'assets/base.png');
		game.load.image('vjoy_body', 'assets/body.png');
		game.load.image('vjoy_cap', 'assets/cap.png');

		game.load.image('virtW', 'assets/virtUp.png');
		game.load.image('virtA', 'assets/virtLeft.png');
		game.load.image('virtS', 'assets/virtDown.png');
		game.load.image('virtD', 'assets/virtRight.png');
		game.load.image('virtSpecial', 'assets/virtS.png');

	}

    // Keeps game running despite going out of focus
    game.stage.disableVisibilityChange = true;

}

function create() {

    if(onMobile){

    	background = game.add.sprite(0, 0, 'background');

    }else{

    	background = game.add.sprite(0,0,'noStarsBackground');
    	stars = game.add.sprite(0,600,'stars');
    	stars.anchor.x = .5;
    	stars.anchor.y = .5;

    }

    // Separation Bar
	separationBar = game.add.sprite(0,0,'separationBar');
	game.physics.arcade.enable(separationBar);
	separationBar.body.immovable = true;
	separationBar.visible = false;
    
    // Audio
    backgroundMusic = game.add.audio('bgMusic');
    backgroundMusic.play();
    pew = game.add.audio('pew');
	deathExplosion = game.add.audio('deathExp');
	damageTaken = game.add.audio('dmgTaken');

    //MainMenu
    gameTitleSprite = game.add.sprite(35,165,'gameTitle');
    gameTitleSprite.scale.setTo(0.9,1.3);
    gameTitleSprite.angle = -10;
    findGameButton = game.add.button(179, 300, 'findGame', findGameClick);
    instructionsButton = game.add.button(300, 450, 'instructions', instructionsClick);
    
    //LookingForPlayerMenu
    lookingForPlayerTitleSprite = game.add.sprite(30,320,'lookingForPlayerTitle');
    lookingForPlayerTitleSprite.scale.setTo(0.65, 0.7);
    lookingForPlayerTitleSprite.angle = -10;
    backButton = game.add.button(10, 10, 'back', backToMainClick);
    lookingForPlayerTitleSprite.visible = false;
    backButton.visible = false;
    
    //InstructionsMenu
    instructionsTitleSprite = game.add.sprite(65, 50, 'instructionsTitle');
    instructionsTitleSprite.scale.setTo(1.2, 0.9);
    instructions2Button = game.add.button(20, 470, 'instructions2', instructionsClick);
    controlsButton = game.add.button(280, 470, 'controls', controlsClick);
    powerUpsButton = game.add.button(540, 470, 'powerUps', powerUpsClick);
    instructionsDescSprite = game.add.sprite(0, 0, 'instructionsDescription');
    selectedButton = game.add.image(20, 470, 'selectedButton');
    selectedButton.visible = false;
    instructionsTitleSprite.visible = false;
    instructions2Button.visible = false;
    controlsButton.visible = false;
    powerUpsButton.visible = false;
    instructionsDescSprite.visible = false;
    
    //ControlsMenu
    controlsTitleSprite = game.add.sprite(125, 50, 'controlsTitle');
    controlsTitleSprite.scale.setTo(1.2, 0.9);
    controlsDescSprite = game.add.sprite(0, 0, 'controlsDescription');
    controlsTitleSprite.visible = false;
    controlsDescSprite.visible = false;
    
    //PowerUpsMenu
    powerUpsTitleSprite = game.add.sprite(125, 50, 'powerUpsTitle');
    powerUpsTitleSprite.scale.setTo(1.2, 0.9);
    powerUpsDescSprite = game.add.sprite(0, 0, 'powerUpsDescription');
    powerUpsTitleSprite.visible = false;
    powerUpsDescSprite.visible = false;
    
    //DefeatMenu
    defeatTitleSprite = game.add.sprite(100,170,'defeatTitle');
    defeatTitleSprite.scale.setTo(1.7, 1.7);
    defeatTitleSprite.angle = -10;
    mainMenuButton = game.add.button(65, 400, 'mainMenu', mainMenuClick);
    newGameButton = game.add.button(405, 400, 'newGame', findGameClick);
    defeatTitleSprite.visible = false;
    mainMenuButton.visible = false;
    newGameButton.visible = false;
    
    //VictoryMenu
    victoryTitleSprite = game.add.sprite(100,170,'victoryTitle');
    victoryTitleSprite.scale.setTo(1.7, 1.7);
    victoryTitleSprite.angle = -10;
    mainMenuButton = game.add.button(65, 400, 'mainMenu', mainMenuClick);
    newGameButton = game.add.button(405, 400, 'newGame', findGameClick);
    victoryTitleSprite.visible = false;
    mainMenuButton.visible = false;
    newGameButton.visible = false;
    
    // Gameplay Screen
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    satellite = game.add.sprite(game.world.width/2, game.world.height/2+25, 'satellite');
    game.physics.arcade.enable(satellite);
    satellite.body.immovable = true;
    satellite.body.setSize(50, 62, 0, 0);
    satellite.scale.setTo(0.5, 0.5);
    satellite.anchor.setTo(0.5, 0.5);
    satellite.visible = false;
    
    rocks = game.add.group();
    rocks.enableBody = true;
    rocks.visible = false;
    
    // create obstacles
    var rock = rocks.create(game.world.width/2-(164/2), 120, 'bigRock');
    rock.body.immovable = true;
    rock = rocks.create(game.world.width/2-(164/2), game.world.height-180, 'bigRock');
    rock.body.immovable = true;
    rock = rocks.create(80, game.world.height - 80, 'smallRock');
    rock.body.immovable = true;
    rock = rocks.create(game.world.width-120, 140, 'smallRock');
    rock.body.immovable = true
    
    // create health bars
    myHealthBar = game.add.sprite(0, 0, 'gHlthBr');  
	myHealthBar.visible = false;
    theirHealthBar = game.add.sprite(800, 0, 'gHlthBr');
    theirHealthBar.scale.x = -1;
	theirHealthBar.visible = false;
    playerHealthText = game.add.text(5, 5, '', {fill: "#ffffff", fontSize: "15px"});
    playerHealthText.visible = false;
    enemyHealthText = game.add.text(715, 5, '', {fill: "#ffffff", fontSize: "15px"});
    enemyHealthText.visible = false;
    blueHealthIndicator = game.add.sprite(0, 5, 'blueShip');
    blueHealthIndicator.visible = false;
    blueHealthIndicator.scale.setTo(.3, .3);
    greenHealthIndicator = game.add.sprite(760, 5, 'greenShip');
    greenHealthIndicator.visible = false;
    greenHealthIndicator.scale.setTo(.3, .3);
    
    // Create Powerups
    // Shield Powerups
    shieldIcon = game.add.sprite(game.world.width/2, game.world.height/2, 'shieldIcon');
    shieldIcon.name = 'shield';
    shieldIcon.visible = false;
    shieldIcon.scale.setTo(0.25, 0.25);
    game.physics.arcade.enable(shieldIcon);
    shieldIcon.body.immovable = true;
    shieldInUse = game.add.image(0, 0, 'shieldInUse');
    shieldInUse.visible = false;
    shieldInUse.scale.setTo(0.7, 0.7);
    shieldInUse.anchor.set(0.5, 0.5);
	// mine powerup
	mineIcon = game.add.sprite(game.world.width/2 + 50, game.world.height/2,'mineIcon');
	mineIcon.visible = false;
	mineIcon.name = 'mine';
	mineIcon.scale.setTo(.25,.25);
	game.physics.arcade.enable(mineIcon);
	mineIcon.body.immovable = true;
	mine = game.add.sprite(0,0,'mineAtlas','mineFadeAnimation.0000.png');
	mine.visible = false;
	mine.anchor.setTo(.5,.5);
	mine.scale.x = .5;
	mine.scale.y = .5;
	mine.color = 'none';
	mine.active = false;
	mineFadeAnimation = mine.animations.add('fade',Phaser.Animation.generateFrameNames('mineFadeAnimation.', 0, 240,'.png',4), 60, true);
	game.physics.arcade.enable(mine);
    
    // Health Powerups
	healthIcon = game.add.sprite(200, 200, 'healthIcon');
    healthIcon.name = 'health';
    healthIcon.visible = false;
    healthIcon.scale.setTo(0.25, 0.25);
    game.physics.arcade.enable(healthIcon);
    healthIcon.body.immovable = true;

    // countdown timer
	countdownTimer = game.add.sprite(400, 300, 'countdownAtlas','countdown.0000.png');
    countdownTimer.anchor.x = .5;
    countdownTimer.anchor.y = .5;
	countdownTimer.visible = false;
	anim = countdownTimer.animations.add('count', Phaser.Animation.generateFrameNames('countdown.', 1, 240,'.png',4), 60, false);
	anim.onComplete.add(startGame, this);
    colorIndicatorText = game.add.text(game.world.centerX , 17, '', {fill: "#ffffff", fontSize: "24px", backgroundColor: "#111111"});
    colorIndicatorText.anchor.set(0.5);
    colorIndicatorText.visible = false;
	
	// explosion animation
	explosion = game.add.sprite(-300,-300,'explosionAtlas','explosion.0000.png');
	explosion.anchor.x = .5;
	explosion.anchor.y = .5;
	explosion.visible = false;
	explosionAnimation = explosion.animations.add('explode', Phaser.Animation.generateFrameNames('explosion.', 0, 60,'.png',4), 60, false);
	
	// create players
	var tempPlayer1 = new Ship('dummy', game, 'blue');
   	var tempPlayer2 = new Ship('dummy', game, 'green');
    tempPlayer1.ship.visible = false;
    tempPlayer2.ship.visible = false;
	playerList.push(tempPlayer1);
	playerList.push(tempPlayer2);
	
	// mobile controls
	if(onMobile){

		// adding the customized module
		game.vjoy = game.plugins.add(Phaser.Plugin.VJoy);

		// adding the buttons, assigning x, y, and the graphic. Also hide the
		//	buttons until game time.
		virtWButton = game.add.button(100, 300, 'virtW');
		virtAButton = game.add.button(0, 400, 'virtA');
		virtSButton = game.add.button(100, 500, 'virtS');
		virtDButton = game.add.button(200, 400, 'virtD');
		virtSpecialButton = game.add.button(100,400,'virtSpecial');
		virtWButton.visible = false;
		virtAButton.visible = false;
		virtSButton.visible = false;
		virtDButton.visible = false;
		virtSpecialButton.visible = false;

		// adding the functions when button is pressed and released
		// virtXDown and virtXUp are used for button presses
		virtWButton.onInputDown.add(function() {virtWDown = true }, this);
		virtAButton.onInputDown.add(function() {virtADown = true }, this);
		virtSButton.onInputDown.add(function() {virtSDown = true }, this);
		virtDButton.onInputDown.add(function() {virtDDown = true }, this);
		virtSpecialButton.onInputDown.add(function() {virtSpecialDown = true }, this);

		virtWButton.onInputUp.add(function() { virtWDown = false}, this);
		virtAButton.onInputUp.add(function() { virtADown = false}, this);
		virtSButton.onInputUp.add(function() { virtSDown = false}, this);
		virtDButton.onInputUp.add(function() { virtDDown = false}, this);
		virtSpecialButton.onInputUp.add(function() {virtSpecialDown = false }, this);

	}

}

function update() {
	
    if(!onMobile)
		stars.rotation += 0.00005;

    // Only Update the players if they are in game
    if(gamestart){
        
		myHealthBar.visible = true;
		theirHealthBar.visible = true;
		
        //console.log('function update(): gamestart = true');
        
        if(myColor == 'blue'){

            player = playerList[0];
            opponent = playerList[1];
        }
        else{
            
            player = playerList[1];
            opponent = playerList[0];
        }
        
        // Update enemy ship with info from the server
        if(tempEnemyX > -1 && tempEnemyY > -1){

            opponent.ship.rotation = tempEnemyRotation;
            opponent.ship.x = tempEnemyX;
            opponent.ship.y = tempEnemyY;
            opponent.ship.body.velocity.x = tempEnemyVelocity[0];
            opponent.ship.body.velocity.y = tempEnemyVelocity[1];
            opponent.currentPowerUp = tempEnemyPowerUps[0];
            opponent.usingPowerUp = tempEnemyPowerUps[1];
            
            // Update Enemy Power Up Effects
            if(opponent.usingShield()){
                
                shieldInUse.visible = true;
                shieldInUse.x = opponent.ship.x;
                shieldInUse.y = opponent.ship.y;
            }
            else if(!player.usingShield()){
                
                shieldInUse.visible = false;
            }
            
            if(opponent.currentPowerUp == 'shield'){
                
                shieldIcon.visible = false;

            }else if(opponent.currentPowerUp == 'mine'){
				
				mineIcon.visible = false;
				
            }
            else if(opponent.currentPowerUp == 'health'){
                
                healthIcon.visible = false;
            }

        }

        // Update enemy bullets with info from the server
        for(var i in tempEnemyBullets){

            if(tempEnemyBullets[i] != -1){

                var enemyBullet;
                enemyBullet = opponent.bullets.children[i];
                enemyBullet.reset(tempEnemyBullets[i][0], tempEnemyBullets[i][2]);
                enemyBullet.body.velocity.x = tempEnemyBullets[i][1];
                enemyBullet.body.velocity.y = tempEnemyBullets[i][3]; 
            }
        }

        // Update Health Graphic
        playerHealthText.text = 'Health: ' + player.ship.health;
        enemyHealthText.text = 'Health: ' + opponent.ship.health;
        switch(player.ship.health){

            case 100:
                myHealthBar.x = 0;
                myHealthBar.loadTexture('gHlthBr');
                break;
            case 90:
                myHealthBar.x = -30;
                myHealthBar.loadTexture('gHlthBr');
                break;
            case 80:
                myHealthBar.x = -60;
                myHealthBar.loadTexture('gHlthBr');
                break;
            case 70:
                myHealthBar.x = -90;
                myHealthBar.loadTexture('yHlthBr');
                break;
            case 60:
                myHealthBar.x = -120;
                myHealthBar.loadTexture('yHlthBr');
                break;
            case 50:
                myHealthBar.x = -150;
                myHealthBar.loadTexture('yHlthBr');
                break;
            case 40:
                myHealthBar.x = -180;
                myHealthBar.loadTexture('yHlthBr');
                break;
            case 30:
                myHealthBar.x = -210;
                myHealthBar.loadTexture('rHlthBr');
                break;
            case 20:
                myHealthBar.x = -240;
                myHealthBar.loadTexture('rHlthBr');
                break;
            case 10:
                myHealthBar.x = -270;
                myHealthBar.loadTexture('rHlthBr');
                break;
            default:
                myHealthBar.x = -300;
                myHealthBar.loadTexture('rHlthBr');
                break;

        }

        switch(opponent.ship.health){

            case 100:
                theirHealthBar.x = 800;
                theirHealthBar.loadTexture('gHlthBr');
                break;
            case 90:
                theirHealthBar.x = 830;
                theirHealthBar.loadTexture('gHlthBr');
                break;
            case 80:
                theirHealthBar.x = 860;
                theirHealthBar.loadTexture('gHlthBr');
                break;
            case 70:
                theirHealthBar.x = 890;
                theirHealthBar.loadTexture('yHlthBr');
                break;
            case 60:
                theirHealthBar.x = 920;
                theirHealthBar.loadTexture('yHlthBr');
                break;
            case 50:
                theirHealthBar.x = 950;
                theirHealthBar.loadTexture('yHlthBr');
                break;
            case 40:
                theirHealthBar.x = 980;
                theirHealthBar.loadTexture('yHlthBr');
                break;
            case 30:
                theirHealthBar.x = 1010;
                theirHealthBar.loadTexture('rHlthBr');
                break;
            case 20:
                theirHealthBar.x = 1040;
                theirHealthBar.loadTexture('rHlthBr');
                break;
            case 10:
                theirHealthBar.x = 1070;
                theirHealthBar.loadTexture('rHlthBr');
                break;
            default:
                theirHealthBar.x = 1100;
                theirHealthBar.loadTexture('rHlthBr');
                break;

        }

        // Move Satellite Obstacle
        satellite.rotation += 0.025;
        
        // Check player collisions
        // player and obstacles
        game.physics.arcade.collide(player.ship, rocks);
		game.physics.arcade.collide(player.ship, separationBar);
		game.physics.arcade.collide(player.bullets, separationBar);
        
        // bullets and obstacles
        game.physics.arcade.collide(player.bullets, rocks);
        game.physics.arcade.collide([player.bullets, player.ship], satellite);
        // ship and ship
        game.physics.arcade.collide(player.ship, opponent.ship, function(ship1,ship2){
            ship1.body.velocity.x = 0;
            ship1.body.velocity.y = 0;
            ship2.body.velocity.x = 0;
            ship2.body.velocity.y = 0;});
        
        // player bullets and ships
        game.physics.arcade.collide(player.bullets, player.ship, player.bulletHitPlayer, null, player);
        game.physics.arcade.collide(player.bullets, opponent.ship, opponent.bulletHitPlayer, null, opponent);
        
        // opponent bullets and both ships
        game.physics.arcade.collide(opponent.bullets, player.ship, player.opponentBulletHit, null, player);
        game.physics.arcade.collide(opponent.bullets, opponent.ship, opponent.opponentBulletHit, null, opponent);
        
        // ships and shield powerUp
        game.physics.arcade.collide(player.ship, shieldIcon, player.gainPowerUp, function(ship, powerUp){
            if(this.currentPowerUp == false)
                return true;
            else
                return false; }, player);
        game.physics.arcade.collide(opponent.ship, shieldIcon, function(ship, shield){ shield.kill(); }, function(ship, powerUp){
            if(this.currentPowerUp == false)
                return true;
            else
                return false; }, opponent);
        
        // ships and health powerUp
        game.physics.arcade.collide(player.ship, healthIcon, player.gainPowerUp, function(ship, powerUp){
            if(this.currentPowerUp == false)
                return true;
            else
                return false; }, player);
        game.physics.arcade.collide(opponent.ship, healthIcon, function(ship, healthPowerUp){ healthPowerUp.kill(); }, function(ship, powerUp){
            if(this.currentPowerUp == false)
                return true;
            else
                return false; }, opponent);
			
		// ships and mine icon
		
		game.physics.arcade.collide(player.ship, mineIcon, player.gainPowerUp, function(ship, mineIcon){
            if(this.currentPowerUp == false)
                return true;
            else
                return false; }, player);
		
        game.physics.arcade.collide(opponent.ship, mineIcon, function(ship, mineIcon){ mineIcon.kill(); }, function(ship, mineIcon){
            if(this.currentPowerUp == false)
                return true;
            else
                return false; }, opponent);
			
		game.physics.arcade.collide(player.ship, mine, player.mineHitPlayer ,function(ship, mine){
			
			// if the mine is mine, their health goes down.
			var isMineEnemyMine = (mine.color != myColor);
			//console.log('COLLIDE WITH MINE!!!!: mine.color == myColor = ' + isMineMine.toString());
			return isMineEnemyMine;
			
			}, player);
		
        // Update the player
        if (player.alive){
        
            player.update();
        }

        // Gather bullet coordinates to send to the server
        var bulletCoordinates = [];
        for(i = 0; i < player.bullets.children.length; ++i){
            if(player.bullets.children[i].exists == true){
                bulletCoordinates.push([player.bullets.children[i].x, player.bullets.children[i].body.velocity.x, player.bullets.children[i].y, player.bullets.children[i].body.velocity.y]);
            }
            else{
                bulletCoordinates.push(-1);
            }
        }
        
		if(satellite.body.x < 100){
			
			satellite.body.velocity.x = satelliteVelocity;
			
		}else if(satellite.body.x > 700){
			
			satellite.body.velocity.x = -satelliteVelocity;
			
		}
		
		if(mineBlewUp) mine.kill();
        //console.log('Before Emit. '+myColor+' X: '+player.ship.x+'My Y:'+player.ship.y);
        // Sending the player's updated info
		socket.emit('clientUpdated', {clientColor: myColor,
                                     angleRadians: player.ship.rotation,
									 xCoordinate: player.ship.x,
									 yCoordinate: player.ship.y,
                                     xVelocity: player.ship.body.velocity.x,
									 yVelocity: player.ship.body.velocity.y,
                                     bCoordinates: bulletCoordinates,
                                     gameID: myGameID,
                                     powerUpInfo: [player.currentPowerUp, player.usingPowerUp],
									 mineInfo: [mine.x, mine.y, mine.color, mine.active]
                                     });

    }
}

//////////////////////////////////////////////////////////
// Menu Screen Button Functions
//////////////////////////////////////////////////////////

function findGameClick(){
    gameTitleSprite.visible = false;
    findGameButton.visible = false;
    instructionsButton.visible = false;
    defeatTitleSprite.visible = false;
    mainMenuButton.visible = false;
    newGameButton.visible = false;
    victoryTitleSprite.visible = false;
    
    lookingForPlayerTitleSprite.visible = true;
    backButton.visible = true;
    
    socket.emit('needColor', socketID);
}

function instructionsClick(){
    gameTitleSprite.visible = false;
    findGameButton.visible = false;
    instructionsButton.visible = false
    controlsTitleSprite.visible = false;
    powerUpsTitleSprite.visible = false;
    controlsTitleSprite.visible = false;
    controlsDescSprite.visible = false;
    powerUpsDescSprite.visible = false;
    
    backButton.visible = true;
    instructionsTitleSprite.visible = true;
    instructions2Button.visible = true;
    controlsButton.visible = true;
    powerUpsButton.visible = true;
    instructionsDescSprite.visible = true;
    selectedButton.reset(instructions2Button.x, instructions2Button.y);
}

function backToMainClick(){
	separationBar.visible = false;
    lookingForPlayerTitleSprite.visible = false;
    backButton.visible = false;
    instructionsTitleSprite.visible = false;
    instructions2Button.visible = false;
    controlsButton.visible = false;
    powerUpsButton.visible = false;
    controlsTitleSprite.visible = false;
    powerUpsTitleSprite.visible = false;
    instructionsDescSprite.visible = false;
    controlsDescSprite.visible = false;
    powerUpsDescSprite.visible = false;
    selectedButton.visible = false;
    
    gameTitleSprite.visible = true;
    findGameButton.visible = true;
    instructionsButton.visible = true;
    
}

function controlsClick(){
    instructionsTitleSprite.visible = false;
    powerUpsTitleSprite.visible = false;
    instructionsDescSprite.visible = false;
    powerUpsDescSprite.visible = false;
    
    controlsTitleSprite.visible = true;
    controlsDescSprite.visible = true;
    selectedButton.reset(controlsButton.x, controlsButton.y);
}

function powerUpsClick(){
    instructionsTitleSprite.visible = false;
    controlsTitleSprite.visible = false;
    instructionsDescSprite.visible = false;
    controlsTitleSprite.visible = false;
    controlsDescSprite.visible = false;
    
    powerUpsTitleSprite.visible = true;
    powerUpsDescSprite.visible = true;
    selectedButton.reset(powerUpsButton.x, powerUpsButton.y);
}

function mainMenuClick(){
    defeatTitleSprite.visible = false;
    mainMenuButton.visible = false;
    newGameButton.visible = false;
    backButton.visible = false;
    victoryTitleSprite.visible = false;
    
    gameTitleSprite.visible = true;
    findGameButton.visible = true;
    instructionsButton.visible = true;
    
}
    
function startGame(){
    
    // putting virtual controls on the screen
    if(onMobile){
	    // enabling the virtual controls
		virtWButton.visible = true;
		virtAButton.visible = true;
		virtSButton.visible = true;
		virtDButton.visible = true;
		virtSpecialButton.visible = true;
		// 400 signifies that it can be activated on the right side of the screen
		game.vjoy.inputEnable(400);
	}

	// resetting the satellite
    satellite.x = 400;
	satellite.body.velocity.x = -satelliteVelocity;

	// resetting the mine
	mineBlewUp = false;
	mine.active = false;
	mine.color = 'none';
	mineIcon.visible = true;
	mine.active = false;

	// Turning on the GUI, obstacles and players
	separationBar.visible = true;
    colorIndicatorText.visible = false;
    playerHealthText.visible = true;
    enemyHealthText.visible = true;
    blueHealthIndicator.visible = true;
    greenHealthIndicator.visible = true;
    rocks.visible = true;
    satellite.visible = true;
    shieldIcon.reset(game.world.width/2, game.world.height/2);
    healthIcon.reset(200, 200);
    for(var i in playerList){
        playerList[i].ship.visible = true;
    }
    // Show initial Health
    if(myColor == 'blue'){
        playerHealthText.text = 'Health: ' + playerList[0].ship.health;
        enemyHealthText.text = 'Health: ' + playerList[1].ship.health;
    }
    else{
        playerHealthText.text = 'Health: ' + playerList[1].ship.health;
        enemyHealthText.text = 'Health: ' + playerList[0].ship.health;
    }
    myHealthBar.visible = true;
    theirHealthBar.visible = true;

    // disabling the menu
    backButton.visible = false;
    lookingForPlayerTitleSprite.visible = false;

    gamestart = 1;
    
}

function showDefeat(){

    console.log('Defeat Screen');
    gameTitleSprite.visible = false;
	findGameButton.visible = false;
	instructionsButton.visible = false;
    rocks.visible = false;
    satellite.visible = false;
    shieldIcon.reset(game.world.width/2, game.world.height/2);
    shieldIcon.visible = false;
    shieldInUse.visible = false;
    healthIcon.reset(200, 200);
    healthIcon.visible = false;
    playerList[0].ship.visible = false;
    playerList[1].ship.visible = false;
    blueHealthIndicator.visible = false;
    greenHealthIndicator.visible = false;
    playerHealthText.text = '';
    enemyHealthText.text = '';

	defeatTitleSprite.visible = true;
	mainMenuButton.visible = true;
	newGameButton.visible = true;
}

function showVictory(){

    console.log('Victory Screen');
	gameTitleSprite.visible = false;
	findGameButton.visible = false;
	instructionsButton.visible = false;
    rocks.visible = false;
    shieldIcon.reset(game.world.width/2, game.world.height/2);
    shieldIcon.visible = false;
    shieldInUse.visible = false;
    healthIcon.reset(200, 200);
    healthIcon.visible = false;
    playerList[0].ship.visible = false;
    playerList[1].ship.visible = false;
    blueHealthIndicator.visible = false;
    greenHealthIndicator.visible = false;
    playerHealthText.text = '';
    enemyHealthText.text = '';

	victoryTitleSprite.visible = true;
	mainMenuButton.visible = true;
	newGameButton.visible = true;

}

setInterval(function(){
	
	//console.log('mine.active: ' + mine.active);
	//console.log('mineBlewUp: ' + mineBlewUp.toString());
	//console.log('mine.color: ' + mine.color);
	
},5000);
