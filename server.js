//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Server Classes
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function Client(sockID){
	
	this.socketID = sockID;
	this.playerColor = 0;
	
}

function Player(sockID, plyrColor){
	
	this.socketID = sockID;
	this.playerColor = plyrColor;
	
}

function Game(blueClient, greenClient, gID){

	console.log('before defining');
	console.log(blueClient.socketID);
	console.log(greenClient.socketID);
	//console.log(this.gameID);
	this.blueSocketID = blueClient.socketID;
	this.greenSocketID = greenClient.socketID;
	this.gameID = gID;
    this.blueHealth = 100;
    this.greenHealth = 100;
	console.log('after defining');
	console.log(this.blueSocketID);
	console.log(this.greenSocketID);
	console.log(this.gameID);

}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Server Variables
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var players = [];				// players is for connections that want a game
var games = [];					// games is for ACTIVE games with two players
var connections = [];			// all the socket connections.
var numberOfGames = -1;			// numberOfGames will be the id's that games are
								//	are assigned.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Setting up server
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Give the client the initial page to start the game
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

// Allowed so that other pages/content can be hosted.
app.use(express.static(__dirname + '/static'));
http.listen(34534, function(){
  console.log('listening on *:34534');
});

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Server Actions
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// handles when a player connects and performs actions
io.on('connection', function(socket){

	var client = new Client(socket.id);
	connections.push(client);
	console.log('a client connected: ' + client.socketID + ', ' +
				 connections.length + ' clients online.');
	io.emit('clientConnected', client.socketID);

	// handles when a player disconnects
	socket.on('disconnect',function(){

		tempSocketID = socket.id;
		for(var i = 0; i < connections.length; i++){
			
			// find the client socket that disconnected and then remove it
			// using splice
			if(tempSocketID == connections[i].socketID){

				// Check to see if the client that disconnected was playing a
				// game. If so, send the other player a victory message. Also,
				// remove the game from the game array.
				for(var j = 0; j < games.length; j++){

					if(games[j].blueSocketID == tempSocketID){

						io.to(games[j].greenSocketID).emit('gameEnded', 'blue');

					}else if(games[j].greenSocketID == tempSocketID){

						io.to(games[j].blueSocketID).emit('gameEnded', 'green');

					}
					games.splice(j,1);
					break;

				}
				connections.splice(i,1);
				console.log('Client ' + tempSocketID + 
							' disconnected from the server. There are now '+
							connections.length + ' clients connected.');
				
			}
			
		}

	}); // socket.on('disconnect');

	socket.on('needColor', function(sock){
		
		console.log('color requested.');
		var playerColor;
		var tempPlayer;
		// first player is blue, second to connect is green
		if((players.length % 2) == 0){
			
			playerColor = 'blue';
			
		}else{
			
			playerColor = 'green';
			
		}

		// Book keeping for server side
		// adds color to the connection
		for(var i = 0; i < connections.length; i++){

			if(connections[i].socketID == sock){
				
				console.log('Updating player color!');
				connections[i].playerColor = playerColor;

			}

		}

		tempPlayer = new Player(sock, playerColor);
		console.log('New Player Color: ' + tempPlayer.playerColor);
		players.push(tempPlayer);
		console.log('players[' + (players.length - 1) + '].socketID = ' +
						players[players.length - 1].socketID);

		// tell client their color
		io.to(sock).emit('colorAssignment', playerColor);

		// If we have enough players, add a game to the game array AND tell
		// clients to start their game.
		if(players.length == 2){

			console.log('players[0].socketID = ' + players[0].socketID);
			console.log('players[1].socketID = ' + players[1].socketID);
			var blueSocketID = players[0].socketID;
			var greenSocketID = players[1].socketID;
			numberOfGames++;
			var tempGame = new Game(players[0], players[1], numberOfGames);
			console.log('Game Created! gameID: ' + tempGame.gameID +
				' blueSocketID: ' + tempGame.blueSocketID + ' greenSocketID: ' +
				tempGame.greenSocketID);
			games.push(tempGame);

			// Send to the players that their game has started.
			io.to(blueSocketID).emit('gameStarted', tempGame.gameID);
			io.to(greenSocketID).emit('gameStarted', tempGame.gameID);

			// clear the waiting queue.
			players = [];

		}
		
	}); // socket.on('needColor')

	socket.on('clientUpdated', function(message){
		
		//console.log('clientUpdated: got update from game: ' + message.gameID);
		// Get gameID to send back to the proper clients
		var tempGameID = message.gameID;
		var gameIndex = -1;

		// Game ID's from the games array, simplifies sending process.
		var tempBlueSocketID = 0;
		var tempGreenSocketID = 0;

		// Search through the games array for this game. When we have a match
		// assign the ID's
		for(var i = 0; i < games.length; ++i){

			if(games[i].gameID == tempGameID){

				gameIndex = i;
				tempBlueSocketID = games[i].blueSocketID;
				tempGreenSocketID = games[i].greenSocketID;
				//console.log('tempBlueSocketID = ' + games[i].blueSocketID);
				//console.log('tempGreenSocketID = ' + games[i].greenSocketID);
				break;

			}

		}

		if(tempBlueSocketID == 0 || tempGreenSocketID == 0 || gameIndex == -1){

			console.log('ERROR: Game was not found!');

		}

		// color that we got from the message, will be a duplicate of one of the
		// two above variables.
		var color = message.clientColor;

        // sending coordinates
        var blue_info = [];
        var green_info = [];
        var blue_bullets = [];
        var green_bullets = [];
        if(color == 'blue'){

            blue_info = [message.xCoordinate, message.xVelocity,
                            message.yCoordinate, message.yVelocity,
                            message.angleRadians, message.powerUpInfo];
            blue_bullets = message.bCoordinates;

        }else{

            green_info = [message.xCoordinate, message.xVelocity,
                            message.yCoordinate, message.yVelocity,
                            message.angleRadians, message.powerUpInfo];
            green_bullets = message.bCoordinates;

        }
        var data = [blue_info, blue_bullets, green_info,
                             green_bullets, message.mineInfo];
		
		
        io.to(tempBlueSocketID).emit('updatedState', data);
        io.to(tempGreenSocketID).emit('updatedState', data);

	}); //socket.on('clientUpdated')
    
    socket.on('healthChange', function(message){
        
        var tempGameID = message[0];
        var playerColorToUpdate = message[1];
        var healthModifier = message[2];
        var gameOver = false;
        
        // Find the current game
        var game;
        var gameIndex;
        for(var i = 0; i < games.length; ++i){

			if(games[i].gameID == tempGameID){

                gameIndex  = i;
				game = games[i];
				tempBlueSocketID = games[i].blueSocketID;
				tempGreenSocketID = games[i].greenSocketID;
				break;
			}
		}

		if(tempBlueSocketID == 0 || tempGreenSocketID == 0 || i == games.length){
            
			console.log('ERROR: Game was not found when health Updated!');
		}

		// if the damage was done by the mine, signal to both in server
		console.log('healthModifier = ' + healthModifier)
		if(healthModifier == -30){
			
			io.to(tempBlueSocketID).emit('mineBlewUp', 'dummy');
			io.to(tempGreenSocketID).emit('mineBlewUp', 'dummy');
			
		}
		
        // Update the player's health
        if(playerColorToUpdate == 'blue'){
            
            game.blueHealth += healthModifier;
            
            // Check for max health
            if(game.blueHealth > 100){
                
                game.blueHealth = 100;
            }
            
            console.log('blueHealth: ' + game.blueHealth);
            
            // Check for gameover
            if(game.blueHealth <= 0){
                
                console.log('The blue player has died.');
                gameOver = true;
                
                // communicating the game is over
                io.to(tempBlueSocketID).emit('gameEnded', 'blue');
                io.to(tempGreenSocketID).emit('gameEnded', 'blue');
            }
            
        }
        else if(playerColorToUpdate == 'green'){
            
            game.greenHealth += healthModifier;
            
            // Check for max health
            if(game.greenHealth > 100){
                
                game.greenHealth = 100;
            }
            
            console.log('greenHealth: ' + game.greenHealth);
            
            // Check for gameover
            if(game.greenHealth <= 0){
                
                console.log('The green player has died.');
                gameOver = true;
                
                // communicating the game is over
                io.to(tempBlueSocketID).emit('gameEnded', 'green');
                io.to(tempGreenSocketID).emit('gameEnded', 'green');
            }
            
        }
        
        io.to(tempBlueSocketID).emit('healthUpdated', [game.blueHealth, game.greenHealth]);
        io.to(tempGreenSocketID).emit('healthUpdated', [game.blueHealth, game.greenHealth]);
        
        // If a game has ended, send to appropriate socket 'game ended' AND remove
        // the game in the games array AND clear the color for the connection
		if(gameOver == true){

			// removing from game array
			games.splice(gameIndex,1);

			// removing color from connections
			for(var i = 0; i < connections.length; i++){

				if(connections[i].socketID == tempBlueSocketID){

					connections[i].playerColor = 0;
				}

				if(connections[i].socketID == tempGreenSocketID){

					connections[i].playerColor = 0;
				}
			}
		}
        
    });

}); //io.on('connection')

// Happens every 5s good tool for debugging
setInterval(function(){
	
	
	
},5000);