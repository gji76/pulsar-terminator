

// Phaser functions
preload()	// Loads resources onto the screen when first launched
create()	// Creates the correct amount of objects defined below
update()	// Updates the state of the game by processing user input and calling functions below

// Game Pieces classes
class Player{
	
	// Data members
	x					// x coordinate of player
	y					// y coordinate of player
	angle = 0			// direction that the ship is facing
	ammoCount = 5		// amount of ammo available. Max is 5
	health = 100 		// Player health. Max is 100
	powerUp = 0 		// Number corresponds to which power up they hold.
	usingPower = 0		// Indicates if a power up is in use
	
	// Player Functions
	Player constructor(int x, int y, int angle)		// creates a player object
	void move(int x, int y)							// move players position
	void dodge()									// move players position at a faster speed
	void shoot() 									// creates a bullet object and decrements ammo count
	void gainHealth()								// player health is increased
	void gainAmmo()									// player ammo is increased
	void gainPowerUp(int powerUpId)					// gives the player the ability to use a powerUp
	void usePowerUp()								// starts the power ups ability
	void hitByProjectile(string projectile)			// Subtract health
	
}

class PowerUp{
	
	// Data members
	x 				// x coordinate of player
	y				// y coordinate of player
	id = 0 			// Number corresponds to which kind of power up this is.
	time = 0		// Amount of time the power up is in effect once used
	
	// Power Up Functions
	constructor(int x, int y)	// creates a power up object
	void move(int x, int y)		// move the power up's location
	void startPower()			// starts the power ups ability
	
}

class Projectile{
	
	// Data members
	x				// x coordinate of player
	y				// y coordinate of player
	angle			// angle the projectile is moving in
	type 			// Bullets, laser, invincible bullets
	
	// Projectile Functions
	Projectile constructor(int x, int y, int angle,int type)	// creates a Projectile object
	void move(int x,int y)										// move the projectile at a constant rate 
	void hitPlayer(Player player)								// called when a projectile hits a player. Subtracts health
	
}
	
class GameStats{
	
	void MyHealthBar(int health)		// show the corresponding image to relflect player 1's health
	void EnemyHealthBar(int health)		// show the corresponding image to relflect player 2's health
	void showPower(int id)				// puts the corresponding indicator on the player's ammo bar
	void showAmmoCount(int ammoCount)	// shows how much ammo the player has in their ammo bar
	
}


